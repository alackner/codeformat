from pycparser import c_lexer

class CodeParser:
    def parse_ast(self, filepath):
        code = self.get_code(filepath)
        lexer = c_lexer.CLexer(self.lex_error, self.lex_on_lbrace, self.lex_on_rbrace, self.lex_resolveType)
        lexer.build()
        lexer.input(code)

        while t:=lexer.token():
            print(t)

        return 0

    def lex_error(self, error, line, col):
        print(f"lex error ({line},{col}): {error}")
    
    def lex_on_lbrace(self):
        print("lex on lbrace")

    def lex_on_rbrace(self):
        print("lex on rbrace")
    
    def lex_resolveType(self, typeName):
        print(f"--> Is {typeName} a type?")
        return False
    
    def get_code(self, filepath):
        with open(filepath, "r") as codefile:
            code = ''.join(codefile.readlines())
        return code
        

parser = CodeParser()
parser.parse_ast("./tests/examples/example_function.c")