from codeformat.codeparser import CodeParser
import codeparser

class Formatter:
    def format(inputFile, outputFile):
        fileAst = CodeParser.parse_ast(inputFile)